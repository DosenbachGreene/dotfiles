# workbench
export path=(/usr/local/pkg/workbench/bin_linux64 $path)

# export local bin to PATH
export path=(${HOME}/.local/bin $path)

# AFNI
export AFNIDIR=/usr/local/pkg/afni
export path=($AFNIDIR $path)

# FSL
export FSLDIR=/usr/local/pkg/fsl/
source ${FSLDIR}/etc/fslconf/fsl.sh

# Freesurfer 7.1
export FREESURFER_HOME=/usr/local/pkg/freesurfer7.1
source $FREESURFER_HOME/SetUpFreeSurfer.sh > /dev/null
export SUBJECTS_DIR=${GMT}/${USER}/subjects_dir

# ANTs
export path=(/usr/local/pkg/ANTs/bin $path)

# 4dfp from raichle mount
export RELEASE=/data/nil-bluearc/raichle/lin64-tools
export NILSRC=/data/nil-bluearc/raichle/lin64-nilsrc
export REFDIR=/data/petsun43/data1/atlas
export path=($RELEASE $path)

# add dcm2niix to path
export path=(/opt/dcm2niix $path)

# add convert 3d
export path=(/opt/c3d/bin $path)

# add PICSL_MALF
export path=(/opt/PICSL_MALF $path)

# set THOMAS
export THOMAS_HOME=/opt/thomas_new
export path=($THOMAS_HOME $path)

# set openmp threads
export OMP_NUM_THREADS=16

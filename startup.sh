#!/bin/bash
# This file should be called as the first command of the .bash_profile file
# It can (and should) be installed via the install.sh script

[[ ${ZSH_OVERRIDES_BASH} == 1 ]] && echo "Dotfiles are set to load zsh over bash. Use 'bash --noprofile --norc' to use bash instead."

# get system name
export SYSNAME=$(uname -n)

# setup valid systems to run this script on
export VALIDSYSTEMS=($(ls ~/.dotfiles/systems))

# loop through valid systems
for SYSTEM in ${VALIDSYSTEMS[@]}; do
  SYSTEM=${SYSTEM%.*}
  if [[ ${SYSNAME} == ${SYSTEM} ]]; then # activate zsh if a valid system
    export SHELL=/bin/zsh
    export ZSH_OVERRIDES_BASH=1
    /bin/zsh # call zsh
    exec /bin/true # force quit from calling bash for seemless shell change
  fi
done

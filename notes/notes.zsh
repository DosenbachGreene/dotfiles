# List announcements for users here

echo "$fg[cyan]Update Notes:"
echo ""
echo "$fg[red]04/13/2023 Add THOMAS segmentation to breq."
echo "$fg[red]04/07/2023 All systems are now at freesurfer 7."
echo "$fg[red]04/07/2023 dcm2niix updated to 20220720 version."
echo ""
echo "$fg[red]Check out the 'matlab_log_cleaner' command to clean up leftover MATLAB"
echo "$fg[red]log files in your home directory!"
echo ""
echo "$fg[cyan]This updater does not auto-update your .zshrc and .paths_user files if they"
echo "$fg[cyan]already exist in your home directory. You may want to check the differences between the"
echo "$fg[cyan]versions by using 'diff .zshrc ~/.dotfiles/zshrc' and 'diff .paths_user ~/.dotfiles/paths_user'"
echo "$fg[cyan]and change your files accordingly."
echo ""
echo "$fg[cyan]If you do not know what any of the above means, you can safely ignore the above message."
echo ""
echo "$fg[cyan]Please check out the FAQ: https://gitlab.com/DosenbachGreene/dotfiles#faq"
echo ""
echo "$fg[green]09/21/2023 VERY IMPORTANT UPDATE MESSAGE PLEASE READ THIS:"
echo "$fg[green]The latest dotfiles updates replaces some existing shell plugins"
echo "$fg[green]and overrides your current zshrc file. If you have modified that file"
echo "$fg[green]in any way, you can find a backup copy of it on ~/.zshrc.bak"
echo ""
echo "$fg[cyan]To see these notes again, type \`dotfiles_notes\` in your terminal."
echo ""

